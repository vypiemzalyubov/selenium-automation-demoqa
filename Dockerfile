FROM python:3.11-slim-bullseye
WORKDIR /src
COPY . .
RUN pip3 install -r requirements.txt
CMD ["pytest", "tests/", "--browser=remote", "--headless=true"]
