# Selenium Automation DEMOQA

[![Python](https://img.shields.io/badge/python-3.11-blue)](https://www.python.org/downloads/release/python-3110/)
[![License MIT](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/vypiemzalyubov/selenium-automation-demoqa/-/blob/main/LICENSE?ref_type=heads)

Automation of [DEMOQA](https://demoqa.com/) site testing using Selenium and Page Object Model

## Used technologies
<p align="center">
  <img width="5%" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/PyCharm_Icon.svg/1200px-PyCharm_Icon.svg.png" title="PyCharm" alt="PyCharm">
  <img width="5%" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1869px-Python-logo-notext.svg.png" title="Python" alt="Python">
  <img width="6%" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Pytest_logo.svg/2048px-Pytest_logo.svg.png" title="Pytest" alt="Pytest">
  <img width="5%" src="https://avatars0.githubusercontent.com/u/983927?v=3&s=400" title="Selenium" alt="Selenium">
  <img width="5%" src="https://biercoff.com/content/images/2017/08/allure-logo.png" title="Allure Report" alt="Allure Report">
  <img width="5%" src="https://avatars.githubusercontent.com/u/110818415?s=200&v=4" title="Pydantic" alt="Pydantic">
  <img width="5%" src="https://i.postimg.cc/fbsyvkVW/requests.png" title="Requests" alt="Requests">
  <img width="5%" src="https://cdn.worldvectorlogo.com/logos/gitlab.svg" title="GitLab" alt="GitLab">
  <img width="5%" src="https://future-architect.github.io/images/20230306a/gitlab-ci-cd-logo_2x.png" title="GitLab CI" alt="GitLab CI">
  <img width="6%" src="https://cdn.iconscout.com/icon/free/png-256/free-docker-226091.png?f=webp" title="Docker" alt="Docker">
  <img width="5%" src="https://img.stackshare.io/service/3136/thumb_retina_docker-compose.png" title="Docker Compose" alt="Docker Compose">
  <img width="4%" src="https://docs.astral.sh/ruff/assets/bolt.svg" title="Ruff" alt="Ruff">
</p>

## Project Structure:
```
selenium-automation-demoqa/
├── models
│   ├── models.py
├── pages
│   ├── ...
│   ├── base_page.py
│   ├── elements_page.py
│   ├── ...
├── tests
│   ├── ...
│   ├── conftest.py
│   ├── elements_test.py
│   ├── ...
├── utils
│   ├── driver
│   │   ├── driver.py
│   │   ├── options.py
│   ├── generator.py
│   ├── logger.py
│   ├── routes.py
│   └── settings.py
├── ...
├── .gitlab-ci.yml
├── Dockerfile
└── requirements.txt
```

## Getting Started
```bash
# Clone repository
git clone https://gitlab.com/vypiemzalyubov/selenium-automation-demoqa.git

# Install virtual environment
python3 -m venv venv

# Activate virtual environment
source venv/bin/activate

# Install dependencies
pip install -r requirements.txt
```

## Viewing reports
- Install [**Allure**](https://docs.qameta.io/allure/#_get_started) from the official website
- Generate Allure report
  
  ```bash
  allure serve
  ```

<p align="center">
  <img width="97%" src='https://i.postimg.cc/VLNhHcSj/allure.png' alt='allure'/>
</p>

## GitLab CI
- Go to [**"Run pipeline"**](https://gitlab.com/vypiemzalyubov/selenium-automation-demoqa/-/pipelines/new) in GitLab CI


---

> See this project on [**GitHub**](https://github.com/vypiemzalyubov/selenium-automation-demoqa)
